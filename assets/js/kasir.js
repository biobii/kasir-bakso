var grandTotal = 0;

$(document).ready(function () {

    // alert
    $('div.alert').fadeTo(2000, 500).slideUp(500, function () {
        $('div.alert').slideUp(500);
    });

    // disable tombol bayar di awal
    $('#formSubmit').attr('disabled', true);

    // event pilih menu item
    $('button.menu-item').click(function() {
        let item    = $(this);
        let menuId  = item.attr('menu-id');
        let menuHarga = item.attr('menu-harga');
        let total   = $('#grandTotal');
        let form    = $('#formCheckout');

        if (item.attr('is-buy') == 'false') {
            item.attr('is-buy', 'true'); // ubah state karena item dipilih

            let newElement = `<div class="row form-group item-group" menu-id="${menuId}" menu-nama="${item.text().trim()}" menu-harga="${menuHarga}">
                <div class="col-sm-3">
                    <input id="menu_${menuId}" class="form-control" type="text" value="${item.text().trim()}" disabled>
                    <input type="hidden" name="menu_${menuId}" value="${menuId}">
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="numeric" id="jumlah_menu_${menuId}" value="1" disabled>
                    <input type="hidden" name="jumlah_menu_${menuId}" value="1">
                </div>
                <div class="col-sm-2">
                    <p class="subtotal_item" subtotal_item="${menuHarga}">Rp. <span>${rupiah(menuHarga)}</span></p>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-sm btn-danger btn-remove-item" menu-id="${menuId}">Batal</button>
                </div>
            </div>`;

            $(newElement).insertBefore(total);
            item.attr('subtotal', menuHarga);
        } else {
            let itemGroup       = $(`div.item-group[menu-id="${menuId}"]`);
            let fieldJumlah     = itemGroup.children()
                                    .children(`input#jumlah_menu_${menuId}`);
            let fieldSubtotal   = itemGroup.children()
                                    .children('p.subtotal_item');
            let inputJumlah     = fieldJumlah.next();

            let hargaItem       = parseInt(itemGroup.attr('menu-harga'));
            let jumlahItem      = parseInt(fieldJumlah.val()) + 1; // increment jumlah

            // subtotal
            fieldSubtotal.attr('subtotal_item', hargaItem * jumlahItem);
            fieldSubtotal.children('span').text(rupiah(hargaItem * jumlahItem));
            item.attr('subtotal', hargaItem * jumlahItem);
            let subtotalItem    = parseInt(fieldSubtotal.attr('subtotal_item'));

            fieldJumlah.val(jumlahItem);
            inputJumlah.val(jumlahItem);
        }

        resetPembayaran();
        getGrandTotal();
    });

    // event hapus menu item
    $('#formCheckout').on('click', '.btn-remove-item', function () {
        let menuId   = $(this).attr('menu-id');
        let target  = $(`div.item-group[menu-id="${menuId}"]`);
        let menuItem = $(`button.menu-item[menu-id="${menuId}"]`);

        menuItem.attr('is-buy', 'false');
        menuItem.attr('subtotal', 0);
        target.remove();

        resetPembayaran();
        getGrandTotal();
    });

    // event input pembayaran
    $('input[name="dibayar"]').keyup(function () {
        let _this = $(this);
        let totalDibayar = $('input[name="total_dibayar"]');
        let kembalian = $('small.help-block');
        let input = parseInt($(this).val());
        let hasil = input - grandTotal;
        let buttonBayar = $('#formSubmit');

        if (_this.val() !== '') {
            if (hasil === 0) {
                buttonBayar.attr('disabled', false);
                kembalian.css('visibility', 'visible')
                    .css('color', '#666666')
                    .text('UANG PAS!');
            } else if (hasil > 0) {
                buttonBayar.attr('disabled', false);
                kembalian.css('visibility', 'visible')
                    .css('color', '#1c7430')
                    .text(`KEMBALIAN Rp ${rupiah(hasil)}`);
            } else if (hasil < 0) {
                buttonBayar.attr('disabled', true);
                kembalian.css('visibility', 'visible')
                    .css('color', '#bd2130')
                    .text(`PEMBYARAN KURANG Rp ${rupiah(hasil).substr(1)}`);
            }

            totalDibayar.val(grandTotal);
        } else {
            buttonBayar.attr('disabled', true);
        }
    });

    $('#formReset').click(function () {
        location.reload();
    });

});

function getGrandTotal() {
    grandTotal = 0;
    $('button.menu-item').each(function () {
        let sub = parseInt($(this).attr('subtotal'));
        grandTotal += sub;
    });

    $('#grandTotal').children().text(rupiah(grandTotal));
}

function resetPembayaran() {
    $('input[name="dibayar"]').val('');
    $('small.help-block').css('visibility', 'hidden');
}

function rupiah(num) {
    let rupiah = '';
    let angkarev = num.toString().split('').reverse().join('');
    for (let i = 0; i < angkarev.length; i++) {
        if (i % 3 == 0) {
            rupiah += angkarev.substr(i, 3) + '.';
        }
    }
    return rupiah.split('', rupiah.length - 1).reverse().join('');
}