<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{

    public function get_user($username)
    {
        return $this->db->query('SELECT * FROM admin WHERE username = ?', array($username));
    }

    public function edit_nama($nama, $username)
    {
        return $this->db->query('UPDATE admin SET nama = ? WHERE username = ?', array($nama, $username));
    }

    public function ganti_password($password_baru, $username)
    {
        return $this->db->query('UPDATE admin SET password = ? WHERE username = ?', array($password_baru, $username));
    }

}