<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kasir_model extends CI_Model
{

    public function bayar()
    {
        $input = $this->input->post();
        $fields = array();

        foreach ($input as $key => $item) {
			// menu
            if (substr($key, 0, 5) === 'menu_') {
                $fields['menu'][] = substr($key, 5);
            }

			// jumlah item per menu
            if (substr($key, 0, 12) === 'jumlah_menu_') {
                $fields['jumlah_menu'][substr($key, 12)] = $item; // group jumlah item per menu id
            }
        }

		// total dibayar
        $fields['total_dibayar'] = $input['total_dibayar'];
        $fields['tanggal'] = date('Y/m/d');

        // database transaction
        $this->db->trans_start();
        $this->db->query('INSERT INTO pembelian (total_bayar, tanggal) VALUES (?, ?)',
                            array($fields['total_dibayar'], $fields['tanggal']));
        $pembelian_id = $this->db->insert_id();

        foreach ($fields['jumlah_menu'] as $menu => $jumlah) {
            $res = $this->db->query('SELECT harga FROM menu WHERE id = ?', array($menu))->row();
            $harga = (int) $res->harga;
            
            $this->db->query('INSERT INTO pembelian_item (pembelian_id, menu_id, jumlah, total)
                            VALUES (?, ?, ?, ?)',
                            array($pembelian_id, $menu, $jumlah, $harga * $jumlah));
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return false;
        }
        return true;
    }

    public function hari_ini()
    {
        $tanggal = date('Y/m/d');
        return $this->db->query('SELECT * FROM pembelian WHERE tanggal = ? ORDER BY id DESC', array($tanggal));
    }

    public function rincian_pembelian($id)
    {
        $data['rincian'] = $this->db->query('SELECT pembelian_item.jumlah,
                                pembelian_item.total, menu.nama 
                                FROM pembelian, pembelian_item, menu 
                                WHERE pembelian.id = pembelian_item.pembelian_id 
                                AND menu.id = pembelian_item.menu_id 
                                AND pembelian.id = ?',
                            array($id));
        $data['pembelian'] = $this->db->query('SELECT * FROM pembelian WHERE id = ?', array($id));
        return $data;
    }

    public function laporan($filter)
    {
        return $this->db->query('SELECT * FROM pembelian WHERE tanggal BETWEEN ?  AND ?', array($filter['sejak'], $filter['sampai']));
    }

    public function statistik()
    {
        return $this->db->query('SELECT tanggal, count(id) as jumlah_pembelian FROM pembelian GROUP BY tanggal');
    }

    public function semua_menu()
    {
        return $this->db->query('SELECT * FROM menu');
    }

    public function detail_menu($id)
    {
        return $this->db->query('SELECT * FROM menu WHERE id = ?', array($id));
    }

    public function tambah_menu()
    {
        $this->load->library('form_validation');

        $input = $this->input->post();
        $input['nama'] = ucwords($input['nama']);
        $input['tanggal'] = date('Y/m/d');

        $this->form_validation->set_rules('nama', 'Nama Menu', 'trim|required');
        $this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');

        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_message('numeric', '{field} harus angka.');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('validasi', validation_errors());
            return false;
        }

        $this->db->query('INSERT INTO menu (nama, harga, tanggal) VALUES (?, ?, ?)', $input);
        return true;
    }

    public function edit_menu($id)
    {
        $this->load->library('form_validation');

        $input = $this->input->post();
        $input['nama'] = ucwords($input['nama']);
        $input['tanggal'] = date('Y/m/d');

        $this->form_validation->set_rules('nama', 'Nama Menu', 'trim|required');
        $this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');

        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_message('numeric', '{field} harus angka.');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('validasi', validation_errors());
            return false;
        }

        $this->db->query('UPDATE menu SET nama = ?, harga = ?, tanggal = ? WHERE id = ?',
                            array($input['nama'], $input['harga'], $input['tanggal'], $id));
        return true;
    }

    public function transaksi_menu($id)
    {
        return $this->db->query('SELECT menu_id FROM pembelian_item WHERE menu_id = ?', array($id));
    }

    public function hapus_menu($id)
    {
        if ($this->transaksi_menu($id)->num_rows() > 0) {
            $this->session->set_flashdata('gagal', 'Tidak dapat menghapus menu karena terikat dengan data pembelian.');
            return false;
        }

        $this->db->query('DELETE FROM menu WHERE id = ?', array($id));
        return true;
    }

}