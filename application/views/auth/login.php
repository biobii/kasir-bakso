<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aplikasi Kasir Bakso Lompongan">
    <meta name="author" content="M Faichul Umam">
    <meta name="keywords" content="bakso lompongan, bakso lamongan, bakso, lamongan, kuliner lamongan">

    <!-- Title Page-->
    <title>Masuk</title>

    <!-- Fontfaces CSS-->
    <link href="<?= base_url('assets/css/font-face.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?= base_url('assets/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?= base_url('assets/css/theme.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" media="all">
    <style>
        .body-bg {
            background-color: #f5f5f5;
        }
        .card-box {
            margin-top: 10%;
        }
        .au-card {
            padding-top: 6%;
        }
        h3 {
            font-weight: 500;
            color: #666;
            padding-top: 0;
            text-align: center;
        }
    </style>
</head>
<body class="body-bg">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="card-box col-md-5 col-sm-12">
                <div class="au-card">
                    <h3>Masuk</h3>
                    <hr>

                    <?php if ($this->session->flashdata('gagal')) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $this->session->flashdata('gagal') ?>
                        </div>
                    <?php } ?>

                    <form action="<?= base_url('auth/masuk/submit') ?>" method="post">
                        <div class="form-group">
                            <label><i class="fa fa-user"></i> Username</label>
                            <input type="text" name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-lock"></i> Password</label>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-block">Masuk</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>