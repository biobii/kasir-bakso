<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card">
                                <h3 class="mb-2">Informasi Akun</h3>
                                <hr>

                                <?php if ($this->session->flashdata('sukses')) { ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= trim($this->session->flashdata('sukses')) ?>
                                    </div>
                                <?php } ?>

                                <?php if ($this->session->flashdata('gagal')) { ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= trim($this->session->flashdata('gagal')) ?>
                                    </div>
                                <?php } ?>

                                <form action="<?= base_url('dashboard/akun/simpan') ?>" method="post">
                                    <div class="row form-group">
                                        <div class="col-sm-2">
                                            <label>Nama</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="nama" class="form-control" value="<?= $this->session->auth_nama ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-2">
                                            <label>Username</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="username" class="form-control" value="<?= $this->session->auth_username ?>" title="username tidak dapat diubah" disabled>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info">Simpan</button>
                                </form>

                                <hr>
                                <h3>Ganti Password</h3>
                                <hr>
                                <form action="<?= base_url('dashboard/akun/password') ?>" method="post">
                                    <div class="row form-group">
                                        <div class="col-sm-2">
                                            <label>Password Lama</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="password" name="password_lama" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-2">
                                            <label>Password Baru</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="password" name="password_baru" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>