<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aplikasi Kasir Bakso Lompongan">
    <meta name="author" content="M Faichul Umam">
    <meta name="keywords" content="bakso lompongan, bakso lamongan, bakso, lamongan, kuliner lamongan">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="<?= base_url('assets/css/font-face.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?= base_url('assets/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?= base_url('assets/vendor/css-hamburgers/hamburgers.min.css') ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?= base_url('assets/css/theme.css') ?>" rel="stylesheet" media="all">
    <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" media="all">
    <?php if (uri_string() === 'dashboard/menu' || uri_string() === 'dashboard/hari-ini' || uri_string() === 'dashboard/laporan') { ?>
        <link href="<?= base_url('assets/css/jquery.dataTables.min.css') ?>" rel="stylesheet" media="all">
    <?php } ?>
</head>

<body>
    <div class="page-wrapper">