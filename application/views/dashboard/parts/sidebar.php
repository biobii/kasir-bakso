<!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="<?= base_url('dashboard') ?>">
                        <h3>Dashboard</h2>
                        <!-- <img src="images/icon/logo.png" alt="CoolAdmin" /> -->
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <li>
                        <a href="<?= base_url('dashboard/index') ?>">
                            <i class="fas fa-shopping-cart"></i>Kasir</a>
                    </li>
                    <li>
                        <a href="<?= base_url('dashboard/sekarang') ?>">
                            <i class="fas fa-clock-o"></i>Hari ini</a>
                    </li>
                    <li>
                        <a href="<?= base_url('dashboard/laporan') ?>">
                            <i class="fas fa-calendar-alt"></i>Laporan</a>
                    </li>
                    <li>
                        <a href="<?= base_url('dashboard/menu') ?>">
                            <i class="fas fa-bars"></i>Menu</a>
                    </li>
                    <li>
                        <a href="<?= base_url('dashboard/akun') ?>">
                            <i class="fas fa-user"></i>Akun</a>
                    </li>
                    <li>
                        <a href="<?= base_url('dashboard/logout') ?>">
                            <i class="fas fa-power-off"></i>Log out</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?= base_url('dashboard') ?>">
                    <!-- <img src="images/icon/logo.png" alt="Cool Admin" /> -->
                    <h3>Dashboard</h3>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="<?= base_url('dashboard/index') ?>">
                                <i class="fas fa-shopping-cart"></i> Kasir
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard/sekarang') ?>">
                                <i class="fas fa-clock-o"></i>Hari ini</a>
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard/laporan') ?>">
                                <i class="fas fa-calendar-alt"></i>Laporan</a>
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard/menu') ?>">
                                <i class="fas fa-bars"></i>Menu</a>
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard/akun') ?>">
                                <i class="fas fa-user"></i>Akun</a>
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard/logout') ?>">
                                <i class="fas fa-power-off"></i>Log out</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
<!-- END MENU SIDEBAR-->