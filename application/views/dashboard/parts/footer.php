    </div>
    
 <!-- Jquery JS-->
    <script src="<?= base_url('assets/vendor/jquery-3.2.1.min.js') ?>"></script>
    <!-- Bootstrap JS-->
    <script src="<?= base_url('assets/vendor/bootstrap-4.1/popper.min.js') ?>"></script>
    <script src="<?= base_url('assets/vendor/bootstrap-4.1/bootstrap.min.js') ?>"></script>
    <!-- Vendor JS       -->
    <script src="<?= base_url('assets/vendor/animsition/animsition.min.js') ?>"></script>
    <script src="<?= base_url('assets/vendor/perfect-scrollbar/perfect-scrollbar.js') ?>"></script>
    </script>

    <!-- Main JS-->
    <script src="<?= base_url('assets/js/main.js') ?>"></script>

    <?php if (uri_string() === 'dashboard' || uri_string() === 'dashboard/index') { ?>
        <script src="<?= base_url('assets/js/kasir.js') ?>"></script>
    <?php } ?>
        
    <?php if (uri_string() === 'dashboard/menu' || uri_string() === 'dashboard/hari-ini' || uri_string() === 'dashboard/laporan') { ?>
        <script src="<?= base_url('assets/vendor/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/showDataTable.js') ?>"></script>
    <?php } ?>

</body>

</html>