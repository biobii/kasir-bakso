<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan</title>
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap-4.1/bootstrap.min.css') ?>">
    <style>
        .text-head {
            font-size: 24px;
            text-align: center;
            font-weight: bold;
        }

        .text-date {
            text-align: center;
            font-style: italic;
            font-weight: light;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <h4 class="text-head">Laporan<br>Penjualan Bakso Lompongan</h4>
        <p class="text-muted text-date"><?= $this->input->get('sejak') ?> s/d <?= $this->input->get('sampai') ?></p>
        <!-- <hr> -->
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Pembayaran</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1;
                foreach ($pembelian->result() as $item) { ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $item->tanggal ?></td>
                            <td>Rp. <?= number_format($item->total_bayar, 2, ',', '.') ?></td>
                        </tr>
                    <?php $total += (int)$item->total_bayar;
                } ?>
            </tbody>
        </table>
        <hr>
        <p class="mt-2"><b>Total: Rp. <?= number_format($total, 2, ',', '.') ?></b></p>
    </div>
</body>
</html>