<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-2">
                            <div class="au-card">
                                <h3 class="mb-2">Menu Baru</h3>
                                <hr>

                                <?php if ($this->session->flashdata('validasi')) { ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= trim($this->session->flashdata('validasi')) ?>
                                    </div>
                                <?php } ?>

                                <form action="<?= base_url('dashboard/menu/baru/simpan') ?>" method="post">
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            <label>Nama Menu</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" name="nama" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            <label>Harga</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="numeric" min="1" name="harga" class="form-control" required>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>