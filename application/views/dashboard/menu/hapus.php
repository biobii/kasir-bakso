<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row justify-content-md-center">
                        <div class="col-md-8">
                            <div class="au-card">
                                <h3 class="mb-2">Konfirmasi Hapus Menu</h3>
                                <hr>

                                <div class="alert alert-danger" role="alert">
                                    Anda yakin akan menghapus menu <b><?= $menu->nama ?></b> ?
                                </div>
                                <form action="<?= base_url('dashboard/menu/hapus/' . $menu->id . '/konfirmasi') ?>" method="post">
                                    <a href="<?= base_url('dashboard/menu') ?>" class="btn btn-secondary">Batal</a>
                                    <button type="submit" class="btn btn-danger">Ya, Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>