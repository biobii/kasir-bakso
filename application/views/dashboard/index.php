<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card">
                                <div class="menu-option">
                                    <h3 class="mb-2">Menu</h3>
                                    <hr>

                                    <?php if ($this->session->flashdata('sukses')) { ?>
                                        <div class="alert alert-success" role="alert">
                                            <?= $this->session->flashdata('sukses') ?>
                                        </div>
                                    <?php } ?>

                                    <?php if ($this->session->flashdata('gagal')) { ?>
                                        <div class="alert alert-success" role="alert">
                                            <?= $this->session->flashdata('gagal') ?>
                                        </div>
                                    <?php } ?>
                                    

                                    <!-- fetch menu -->
                                    <?php foreach ($menu->result() as $menu) { ?>
                                        <button class="menu-item" menu-id="<?= $menu->id ?>" menu-harga="<?= $menu->harga ?>" is-buy="false" subtotal="0">
                                            <?= $menu->nama ?>
                                        </button>
                                    <?php } ?>
                                </div>

                                <hr>
                                <h3 class="mt-2">Checkout</h3>
                                <hr>
                                <form id="formCheckout" action="<?= base_url('bayar') ?>" method="post">
                                    <!-- dynamic item here -->
                                    <p id="grandTotal" grand_total="0">
                                        Grand Total:  &nbsp; Rp <span>0</span>
                                    </p>
                                    <hr>
                                    <div id="formPaid" class="row form-group">
                                        <div class="col-sm-2">
                                            <input style="background-color: transparent; border: transparent" type="text" class="form-control" value="Dibayar" disabled>
                                        </div>
                                        <div class="col-sm-3">
                                            <input name="dibayar" type="number" min="0" class="form-control">
                                            <small style="visibility: hidden;" class="help-block form-text">Kembali</small>
                                            <input name="total_dibayar" type="hidden" value="0">
                                        </div>
                                    </div>
                                    <hr>
                                    <button id="formReset" type="button" class="btn btn-danger">Reset</button>
                                    <button id="formSubmit" type="submit" class="btn btn-info">Bayar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>