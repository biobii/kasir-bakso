<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 offset-1">
                            <div class="au-card">
                                <h3 class="mb-2">Rincian Pembelian</h3>
                                <hr>

                                <pre>ID        :  <?= $pembelian->id ?></pre>
                                <pre>Tanggal   :  <?= $pembelian->tanggal ?></pre>
                                <pre>Total     :  Rp. <?= number_format($pembelian->total_bayar, 2, ',', '.') ?></pre>
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Menu</th>
                                                <th>Jumlah</th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($rincian->result() as $item) { ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item->nama ?></td>
                                                    <td><?= $item->jumlah ?></td>
                                                    <td>Rp. <?= number_format($item->total, 2, ',', '.') ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <pre class="pull-right mt-3">Total : Rp. <?= number_format($pembelian->total_bayar, 2, ',', '.') ?></pre>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>