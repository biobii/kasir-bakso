<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card">
                                <h3 class="mb-2">Hari ini</h3>
                                <hr>

                                <div class="table-responsive m-b-40">
                                    <table id="todayTable" class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Tanggal</th>
                                                <th>Pembayaran</th>
                                                <th>Informasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; $total = 0; ?>
                                            <?php foreach ($pembelian->result() as $item) { ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item->tanggal ?></td>
                                                    <td>Rp. <?= number_format($item->total_bayar, 2, ',', '.') ?></td>
                                                    <td>
                                                        <a href="<?= base_url('dashboard/pembelian/rincian/' . $item->id) ?>" class="btn btn-sm btn-success">Rincian</a>
                                                    </td>
                                                </tr>
                                            <?php $total += (int) $item->total_bayar; } ?>
                                        </tbody>
                                    </table>
                                    <p class="mt-2">Total hari ini: Rp. <?= number_format($total, 2, ',', '.') ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>