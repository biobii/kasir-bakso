<?php $this->load->view('dashboard/parts/header') ?>
<?php $this->load->view('dashboard/parts/sidebar') ?>

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <!-- header here -->
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card">
                                <h3 class="mb-2">Daftar Menu</h3>
                                <hr>

                                <?php if ($this->session->flashdata('sukses')) { ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= trim($this->session->flashdata('sukses')) ?>
                                    </div>
                                <?php } ?>

                                <?php if ($this->session->flashdata('gagal')) { ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= trim($this->session->flashdata('gagal')) ?>
                                    </div>
                                <?php } ?>

                                <a href="<?= base_url('dashboard/menu/baru') ?>" class="btn btn-info mb-2">
                                    <i class="fas fa-plus"></i> Menu
                                </a>
                                <div class="table-responsive m-b-40">
                                    <table id="menuTable" class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Menu</th>
                                                <th>Harga</th>
                                                <th>Tanggal</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach ($menu->result() as $item) { ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item->nama ?></td>
                                                    <td>Rp. <?= $item->harga ?></td>
                                                    <td><?= $item->tanggal ?></td>
                                                    <td>
                                                        <a href="<?= base_url('dashboard/menu/edit/' . $item->id) ?>" class="btn btn-sm btn-info">Edit</a>
                                                        <a href="<?= base_url('dashboard/menu/hapus/' . $item->id) ?>" class="btn btn-sm btn-danger">Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php $this->load->view('dashboard/parts/credit') ?>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

<?php $this->load->view('dashboard/parts/footer') ?>