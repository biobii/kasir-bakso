<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        
        if ($this->session->logged_in) {
            return redirect(base_url('dashboard/index'));
        }
    }

    public function index()
    {
        $this->load->view('auth/login');
    }

    public function submit_login()
    {
        $input = $this->input->post();
        $data = $this->admin_model->get_user($input['username']);

        if ($data->num_rows() > 0) {
            $fetch = $data->row();
            $hashed = $fetch->password;

            if (password_verify($input['password'], $hashed)) {
                $user = array(
                    'logged_in' => true,
                    'auth_id' => $fetch->id,
                    'auth_nama' => $fetch->nama,
                    'auth_username' => $fetch->username
                );

                $this->session->set_userdata($user);

                return redirect(base_url('dashboard/index'));
            }
        }

        $this->session->set_flashdata('gagal', 'username atau password salah!');
        return redirect(base_url('auth/masuk'));
    }

}