<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kasir_model');
		$this->load->model('admin_model');

		if (!$this->session->logged_in) {
			return redirect(base_url('auth/masuk'));
		}
	}

	public function index()
	{
		$menu = $this->kasir_model->semua_menu();
		$this->load->view('dashboard/index', array('menu' => $menu));
	}

	public function bayar()
	{
		$status_bayar = $this->kasir_model->bayar();
		
		if ($status_bayar) {
			$this->session->set_flashdata('sukses', 'Pembayaran berhasil!');
		} else {
			$this->session->set_flashdata('gagal', 'Terjadi kesalahan!');
		}
		return redirect(base_url('dashboard/index'));
	}

	public function hari_ini()
	{
		$pembelian = $this->kasir_model->hari_ini();
		$this->load->view('dashboard/today', array('pembelian' => $pembelian));
	}

	public function rincian_pembelian($id)
	{
		$data = $this->kasir_model->rincian_pembelian($id);
		$rincian = $data['rincian'];
		$pembelian = $data['pembelian']->row();

		// jika data ada
		if ($rincian && $pembelian) {
			return $this->load->view(
				'dashboard/pembelian/rincian',
				array(
					'rincian' => $rincian,
					'pembelian' => $pembelian
				)
			);
		}
		
		return redirect(base_url('dashboard/sekarang'));
	}

	public function laporan()
	{
		$input = $this->input->get();
		$total = 0;

		if (! $input || $input['sejak'] === '' || $input['sampai'] === '') {
			$pembelian = null;
			$this->load->view('dashboard/laporan', array('pembelian' => $pembelian, 'total' => $total));
		} else {
			$pembelian = $this->kasir_model->laporan($input);
			$this->load->view('dashboard/laporan', array('pembelian' => $pembelian, 'total' => $total));
		}
	}

	public function laporan_cetak()
	{
		$input = $this->input->get();
		$total = 0;

		if (!$input || $input['sejak'] === '' || $input['sampai'] === '') {
			$pembelian = null;
			$this->load->view('dashboard/laporan', array('pembelian' => $pembelian, 'total' => $total));
		} else {
			$this->load->library('pdf');
			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->filename = 'laporan-' . date('d-m-Y') . '.pdf';

			$pembelian = $this->kasir_model->laporan($input);
			$this->pdf->load_view('dashboard/pdf', array('pembelian' => $pembelian, 'total' => $total));
			// $this->load->view('dashboard/laporan', array('pembelian' => $pembelian, 'total' => $total));
		}	
	}

	public function menu()
	{
		$menu = $this->kasir_model->semua_menu();
		$this->load->view('dashboard/menu', array('menu' => $menu));
	}

	public function menu_baru($submit = null)
	{
		if ($submit === 'simpan') {
			$status = $this->kasir_model->tambah_menu();

			if ($status) {
				$this->session->set_flashdata('sukses', 'Menu berhasil disimpan');
				return redirect(base_url('dashboard/menu'));
			}

			return redirect(base_url('dashboard/menu/baru'));
		} else {
			$this->load->view('dashboard/menu/tambah');
		}
	}

	public function menu_edit($id, $submit = null)
	{
		if ($submit === 'simpan') {
			$status = $this->kasir_model->edit_menu($id);

			if ($status) {
				$this->session->set_flashdata('sukses', 'Menu berhasil diperbarui');
				return redirect(base_url('dashboard/menu'));
			}
			return redirect(base_url('dashboard/menu/edit/' . $id));
		} else {
			$menu = $this->kasir_model->detail_menu($id)->row();
			$this->load->view('dashboard/menu/edit', array('menu' => $menu));
		}
	}

	public function menu_hapus($id, $submit = null)
	{
		if ($submit === 'konfirmasi') {
			$status = $this->kasir_model->hapus_menu($id);
			
			if ($status) {
				$this->session->set_flashdata('sukses', 'Menu berhasil dihapus');
			}

			return redirect(base_url('dashboard/menu'));
		} else {
			$menu = $this->kasir_model->detail_menu($id)->row();
			$this->load->view('dashboard/menu/hapus', array('menu' => $menu));
		}
	}

	public function akun($submit = null)
	{
		if ($submit === 'simpan') {
			$nama = ucwords($this->input->post('nama'));

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_message('required', '{field} tidak boleh kosong!');

			if ($this->form_validation->run() === false) {
				$this->session->set_flashdata('gagal', validation_errors());
			}

			$this->admin_model->edit_nama($nama, $this->session->auth_username);
			$this->session->set_userdata('auth_nama', $nama);
			$this->session->set_flashdata('sukses', 'Pembaruan berhasil disimpan');
			return redirect(base_url('dashboard/akun'));
		} else {
			$this->load->view('dashboard/akun');
		}
	}

	public function ganti_password()
	{
		$input = $this->input->post();
		$data = $this->admin_model->get_user($this->session->auth_username);
		$user = $data->row();

		if (password_verify($input['password_lama'], $user->password)) {
			$hash = password_hash($input['password_baru'], PASSWORD_DEFAULT);
			$this->admin_model->ganti_password($hash, $this->session->auth_username);
			$this->session->set_flashdata('sukses', 'Password baru berhasil disimpan');
		} else {
			$this->session->set_flashdata('gagal', 'Password lama tidak cocok!');
		}
		return redirect(base_url('dashboard/akun'));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		return redirect(base_url());
	}

}
