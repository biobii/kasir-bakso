<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['auth/masuk'] = 'auth/index';
$route['auth/masuk/submit']['post'] = 'auth/submit_login';
$route['bayar']['post'] = 'dashboard/bayar';
$route['dashboard/sekarang'] = 'dashboard/hari_ini';
$route['dashboard/akun/password']['post'] = 'dashboard/ganti_password';
$route['dashboard/pembelian/rincian/(:num)'] = 'dashboard/rincian_pembelian/$1';
$route['dashboard/menu/baru'] = 'dashboard/menu_baru';
$route['dashboard/menu/baru/simpan']['post'] = 'dashboard/menu_baru/simpan';
$route['dashboard/menu/edit/(:num)'] = 'dashboard/menu_edit/$1';
$route['dashboard/menu/edit/(:num)/simpan']['post'] = 'dashboard/menu_edit/$1/simpan';
$route['dashboard/menu/hapus/(:num)'] = 'dashboard/menu_hapus/$1';
$route['dashboard/menu/hapus/(:num)/konfirmasi']['post'] = 'dashboard/menu_hapus/$1/konfirmasi';

$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
